package com.hcloud.common.core.constants;

/**
 * 认证相关的常量
 *
 * @Auther hepangui
 * @Date 2018/10/28
 */
public interface AuthConstants {

    String TOKEN_TYPE_REDIS  = "redis";
    String TOKEN_TYPE_JDBC = "jdbc";
    String TOKEN_TYPE_JWT = "jwt";

    /**
     * jwt签名关键词
     */
    String JWT_SIGNKEY = "h-cloud";
    String OAUTH_TOKEN_PERDIX = "h-cloud-token";

    /**
     * 存放到token中的额外信息key值
     */
    String USER = "user", AUTHORITY = "authorities";

    String SEE = "see",ADD="add",EDIT="edit",DEL="del";

    String SYSADMIN = "sysadmin";

    String SYSTEM_PREFIX = "sys-",
            SYSTEM_AUTH_PREFIX = SYSTEM_PREFIX + "auth-",
            SYSTEM_USER_PREFIX = SYSTEM_PREFIX + "user-",
            SYSTEM_ROLE_PREFIX = SYSTEM_PREFIX + "role-";

    String AUDIT_PREFIX = "audit-",
            AUDIT_OPERATE_PREFIX = AUDIT_PREFIX + "operate-",
            AUDIT_LOGIN_PREFIX = AUDIT_PREFIX + "login-";

    String AUTH_PREFIX = "auth-",
            AUTH_CLIENT_PREFIX = AUTH_PREFIX + "client-",
            AUDIT_TOKEN_PREFIX = AUDIT_PREFIX + "token-";

}
