package com.hcloud.common.crud.entity;

import lombok.Data;

import javax.persistence.Transient;

import javax.persistence.MappedSuperclass;
import java.util.List;

/**
 * 基础的treeEntity，字段增加parentId
 * @Auther hepangui
 * @Date 2018/11/5
 */
@Data
@MappedSuperclass
public class BaseTreeEntity extends BaseEntity{
    private String parentId;

    @Transient
    private List<BaseTreeEntity> children;
}
